extends Node

@onready var _viewport : Window = get_tree().root
@onready var _vp_id : RID = _viewport.get_viewport_rid()
@onready var _base_size : Vector2 = Vector2(_viewport.get_visible_rect().size)


func _ready():
	Utils.connect_or_fail(get_tree().root.size_changed, _on_screen_resized)
	_on_screen_resized()


func _on_screen_resized() -> void :
	var _screen_size := Vector2(DisplayServer.window_get_size())

	var _ratio := _screen_size / _base_size
	var _factor : int = max(1, min(floor(_ratio.x), floor(_ratio.y)))
	var _new_base := (_screen_size / float(_factor))
	var _new_base_ceiled := Vector2i(ceil(_new_base.x), ceil(_new_base.y))
	var _new_screen = Rect2(Vector2.ZERO, _new_base_ceiled * _factor)

	RenderingServer.viewport_set_size(_vp_id, _new_base_ceiled.x, _new_base_ceiled.y)
	RenderingServer.call_deferred("viewport_attach_to_screen", _vp_id, _new_screen)
