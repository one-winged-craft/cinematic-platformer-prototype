extends Object
class_name Func


static func run_tests() -> void:
	var inc := func(n: int) -> int:
		return n + 1

	var array_inc := func(ns):
		return ns.map(inc)

	assert(map_keys({ [1, 2]: "a", [3, 4]: "b" }, array_inc).hash() \
			== { [2,3]: "a", [4,5]: "b" }.hash(), "Should map over every keys")

	assert(map_values({ "a": 1, "b":  2 }, inc).hash() \
			== { "a": 2, "b": 3 }.hash(), "Should map over every values")


## Returns a new dictionary that is the merge of the two.
static func merge(
		dict1: Dictionary, dict2: Dictionary, overwrite := false
	) -> Dictionary:
	var result := dict1.duplicate()
	result.merge(dict2, overwrite)
	return result


## Repeat a new array containing the value repeated count times.
static func repeat(count: int, value: Variant) -> Array[Variant]:
	return range(count).map(func(_c: int) -> Variant: return value)


## Map a function to every key in a dictionary, return a new dictionary.
##
## When two keys maps to the same value, the one kept is undefined.
static func map_keys(dict: Dictionary, function: Callable) -> Dictionary:
	var fold := func(result: Dictionary, key: Variant) -> Dictionary:
		return merge(result, { function.call(key): dict.get(key) })

	return dict.keys().reduce(fold, {})


## Map a function to every values in a dictionary, return a new dictionary.
static func map_values(dict: Dictionary, function: Callable) -> Dictionary:
	var fold := func(result: Dictionary, key: String) -> Dictionary:
		return merge(result, { key: function.call(dict.get(key)) })

	return dict.keys().reduce(fold, {})
