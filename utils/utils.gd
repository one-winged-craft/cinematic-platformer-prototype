extends Object
class_name Utils

## Tries to [method Signal.connect] to a [Callable], but asserts the returns
## code is correct in debug builds.
static func connect_or_fail(
			signal_: Signal, callable: Callable, flags := 0
	) -> void:
	var return_code := signal_.connect(callable, flags)
	assert(return_code == 0, "Connection failed: " + error_string(return_code))

