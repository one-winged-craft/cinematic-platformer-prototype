extends Node
class_name Move


## Given a state and an action, return the sequence of moves to apply to a
## player.
static func next_step(current: Dictionary) -> Dictionary:
	assert(current.has("state"))
	assert(current.has("action"))
	assert(current.has("orientation"))
	assert(current.has("cell_size"))

	var state: StringName = current.state
	var orientation: StringName = current.orientation
	var on_finish: Callable = current.get("on_finish", func() -> void: pass)
	var player_moves := []

	match [ current.state, current.action ]:
		[ &"stand", &"step" ]:
			var direction: Vector2 = vector_from_orientation(
					current.orientation
			) * current.cell_size
			player_moves = [{ direction = direction, duration = 0.5 }]
		[ &"stand", &"turn_around" ]:
			player_moves = [{ direction = Vector2.ZERO, duration = 0.5 }]
			orientation = opposite_orientation_from(current.orientation)
		[ &"stand", &"jump" ]:
			player_moves = [{
					direction = Vector2.UP * current.cell_size,
					duration = 0.25,
				}, {
					direction = Vector2.DOWN * current.cell_size,
					duration = 0.25,
			}]
		[ &"stand", &"crouch" ]:
			state = &"crouch"
		[ &"crouch", &"step" ]:
			var direction: Vector2 = vector_from_orientation(
					current.orientation
			) * current.cell_size
			player_moves = [{ direction = direction, duration = 1 }]
		[ &"crouch", &"turn_around" ]:
			player_moves = [{ direction = Vector2.ZERO, duration = 1 }]
			orientation = opposite_orientation_from(
					current.orientation
			)
		[ &"crouch", &"stand" ]:
			state = &"stand"
		var err:
			push_warning("Cannot guess next step from: ", err)

	return {
		state = state,
		orientation = orientation,
		apply = apply_with(on_finish, player_moves),
	}


@warning_ignore(return_value_discarded)
static func apply_with(
		callback: Callable, player_moves: Array
	) -> Callable:
	return func(player: Node, tilemap: TileMap) -> void:
		var tween := player.create_tween()
		var property_from_move := func(move: Dictionary) -> TweenProperty:
			return tween.tween_property(
					player, "position", move.direction, move.duration
			).as_relative()

		player_moves.map(property_from_move)
		tween.tween_callback(callback)


static func opposite_orientation_from(orientation: StringName) -> StringName:
	match orientation:
		ActionHandler.orientation_left:
			return ActionHandler.orientation_right
		ActionHandler.orientation_right:
			return ActionHandler.orientation_left
		var err:
			push_error("Cannot get opposite orientation from: ", err)
			return orientation


static func vector_from_orientation(orientation: StringName) -> Vector2:
	match orientation:
		ActionHandler.orientation_left:
			return Vector2.LEFT
		ActionHandler.orientation_right:
			return Vector2.RIGHT
		var err:
			push_error("Not a valid orientation: ", err)
			return Vector2.ZERO
