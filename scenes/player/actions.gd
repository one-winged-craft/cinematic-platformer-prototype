extends Object
class_name Actions

## The [Actions] class creates a data structure containing the combo and direct
## actions that are used from an [ActionHandler].
##
## This class should be thought as a static module that shall not need to be
## instantiated in order to be useful.

## The list of valid commands.
const forward := &"forward"
const backward := &"backward"
const downward := &"downward"
const upward := &"upward"


## Abstract commands that maps to a direction relative to the player position.
const inward := &"inward"
const outward := &"outward"
const inward_to_inward := {
	inward: forward,
	outward: backward
}
const inward_to_outward := {
	inward: backward,
	outward: forward,
}

## Creates the combos, associating a chain of commands to an action name for
## every states.
##
## To create such combos, we rely on [method gather] and [method state] to
## define the underlying data structure. We're also using [method repeat],
## [method repeat_single] and [method literal] to express recurrent one-to-many
## relationship between chain of commands and actions.
static func combos() -> Dictionary:
	return gather([
		state("stand", [
			repeat_single("run", 1, 4, forward),
			repeat("running_leap", 3, 4, [], forward, [ upward ]),
			repeat("standing_leap", 1, 2, [ upward ], forward, []),
			literal("step_back", [ backward ]),
			literal("hang_down", [ downward ]),
		]),
		state("crouch", [
			repeat_single("roll", 1, 3, forward),
			literal("crawl_backward", [ backward ]),
			literal("hang_down", [ downward ]),
		]),
		relative_states([{
			name = "ledge_inward",
			map = func(chain): return chain.map(func(command):
				return inward_to_inward.get(command, command)),
		}, {
			name = "ledge_outward",
			map = func(chain): return chain.map(func(command):
				return inward_to_outward.get(command, command)),
		},
		], [
			literal("horizontal_leap", [ upward, outward ]),
			literal("drop_inside", [ downward, inward ]),
		]),
	])

## Creates the directs, associating a single command to an action name for
## every states.
##
## Like the [method combos], we rely on [method gather] and [method state] to
## define the data structure. However, we solely depends on [method literal]
## to express the one-to-one relationship between commands and actions.
static func directs() -> Dictionary:
	return gather([
		state("stand", [
			literal("step", forward),
			literal("jump", upward),
			literal("crouch", downward),
			literal("turn_around", backward),
		]),
		state("crouch", [
			literal("stand", upward),
			literal("step", forward),
			literal("turn_around", backward),
		]),
		state("ledge_inward", [
			literal("face_outward", backward),
			literal("climb_up", upward),
			literal("drop_down", downward),
		]),
		state("ledge_outward", [
			literal("face_inward", backward),
			literal("climb_up", upward),
			literal("drop_down", downward),
		]),
	])

## Return a new dictionary merging every dictionaries together.
##
## For duplicate keys, the first one takes precedence.
static func gather(dictionaries: Array[Dictionary]) -> Dictionary:
	return dictionaries.reduce(Callable(Func, &"merge"), {})


## Return a dictionary specifying a state and its related actions.
##
##
static func state(name: String, actions: Array[Dictionary]) -> Dictionary:
	return { name: gather(actions) }


## Return a dictionary specifying a set of states with their related actions.
static func relative_states(
		states: Array[Dictionary], actions: Array[Dictionary]
	) -> Dictionary:
	var fold := func(result: Dictionary, state: Dictionary) -> Dictionary:
		var state_map := func(chains: Dictionary) -> Dictionary:
			return Func.map_keys(chains, state.map)

		return Func.merge(result,
				Func.map_values(state(state.name, actions), state_map)
		)

	return states.reduce(fold, {})


## Return a dictionary associating the commands to its name.
##
## The command might be single when we create a direct, or a list of them, when
## we create a combo.
static func literal(name: String, command: Variant) -> Dictionary:
	return { command: name }


## Like [method repeat], but for a single command that composes the whole chain.
static func repeat_single(
			name: String, from: int, to: int, command: StringName
	) -> Dictionary:
	return repeat(name, from, to, [], command, [])


## Create actions with a name prefixed by numbers between from and to inclusive.
##
## With the start of a chain, the repeated command, and the end of a chain, we
## can make every combination.
##
## For example, [code]repeat("climb", 0, 3, [ f, f ], u, [ f ])[/code] is
## identical to the following structure:
## [codeblock]
## gather([
##     literal("climb0", [ f, f, f ]),
##     literal("climb1", [ f, f, u, f ]),
##     literal("climb2", [ f, f, u, u, f ]),
##     literal("climb3", [ f, f, u, u, u, f ]),
## ])
## [/codeblock]
static func repeat(
		name: String, from: int, to: int,
		start: Array[StringName], command: StringName, end: Array[StringName]
	) -> Dictionary:
	var apply := func(res: Dictionary, count: int) -> Dictionary:
		return Func.merge(res, {
			start + Func.repeat(count, command) + end: name + str(count)
		})
	return range(from, to + 1).reduce(apply, {})
