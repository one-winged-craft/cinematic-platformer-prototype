extends Object
class_name ActionHandler

## The [ActionHandler] translates input events to commands and relates direct
## actions or combo actions to their names.
##
## Its scope doesn't cover player state management (such as stand, crouch, etc)
## nor it is to handle the timing between input and animation delays.
##
## It communicates various information such as the name of a valid combo or the
## reason of a failure through signals (see [signal direct_entered],
## [signal action_cancelled] and [signal combo_entered]).

## Emitted when a combo gets cancelled out during composition.
##
## The reason is either "user" when the user explicitly pressed the cancel
## button while composing a combo, "invalid chain" when the entered chain has
## no associated combo, or "invalid command" when the entered command has no
## associated direct.
signal action_cancelled(reason: StringName)

## Emitted when the user releases the combo key.
##
## The user needs to enter a chain of directions that are associated to an
## action while maintaining the combo key.
signal combo_entered(state: StringName, action: StringName)

## Emitted when the user presses a key associated to a direct action.
signal direct_entered(state: StringName, action: StringName)

const orientation_left := &"left"
const orientation_right := &"right"

var keys := {
	"movement": [ &"left", &"right", &"up", &"down" ],
	"combo": &"combo",
	"cancel": &"cancel",
}
var combos := {}
var directs := {}
var chain: Array[StringName] = []
var is_composing_combo := false


func _init() -> void:
	combos = Actions.combos()
	directs = Actions.directs()


## Process the combo and direct actions made given an InputEvent.
##
## This method depends on [member chain] and [member is_composing_combo] to
## determine the different state needed to create a combo. As such, this method
## shall be used in conjunction with a single [InputEvent] object, as it would
## result in undefined behavior at the [ActionHandler] scope in any other case.
func process_input(
		event: InputEvent, state: String, orientation := &"right"
	) -> void:
	var state_combos = combos.get(state, {})
	var state_directs = directs.get(state, {})

	var pressed_key: Variant = null
	for key in keys.movement:
		if event.is_action_pressed(key):
			pressed_key = key

	var finish_combo := func() -> void:
		is_composing_combo = false
		chain = []

	if event.is_action_pressed(keys.combo):
		is_composing_combo = true
	elif is_composing_combo:
		if event.is_action_pressed(keys.cancel):
			action_cancelled.emit("user")
			finish_combo.call()
		elif event.is_action_released(keys.combo):
			var maybe_combo: Variant = ActionHandler.identify_combo(
					chain, state_combos
			)
			if maybe_combo:
				combo_entered.emit(state, maybe_combo)
			else:
				action_cancelled.emit("invalid chain")
			finish_combo.call()

	if pressed_key != null:
		var command := ActionHandler.direction_from_key(
				orientation, pressed_key
		)

		if is_composing_combo:
			chain = ActionHandler.compose_combo(
					chain, command, state_combos.keys()
			)
		else:
			var maybe_direct: Variant = ActionHandler.identify_direct(
					command, state_directs
			)
			if maybe_direct:
				direct_entered.emit(state, maybe_direct)
			else:
				action_cancelled.emit("invalid command")


## Get the action associated with a combo in the given actions set, if any.
static func identify_combo(
		current_chain: Array, combo_actions: Dictionary
	) -> Variant:
	return combo_actions.get(current_chain)


## Get the action associated with a command in the given actions set, if any.
static func identify_direct(
		command: StringName, direct_actions: Dictionary
	) -> Variant:
	return direct_actions.get(command)


## Translate a input key to a direction relative to the orientation.
##
## Valid keys are for instance left, right, up or down whereas directions are
## forward, backward, upward and downward. Whether "forward" is "left" or
## "right" depends on the orientation and is determined by this method.
##
## [br]Example:
## [codeblock]
## direction_from_key("left", "left") == "forward"
## direction_from_key("right", "left") == "backward"
## direction_from_key("left", "up") == "upward"
## [/codeblock]
static func direction_from_key(
		orientation: StringName, key: StringName
	) -> StringName:
	var forward_or_backward := func(left_or_right: StringName) -> StringName:
		return (
				Actions.forward if orientation == left_or_right
				else Actions.backward
		)

	match key:
		&"up":
			return Actions.upward
		&"down":
			return  Actions.downward
		var left_or_right:
			return forward_or_backward.call(left_or_right)


## Add a direction to a chain of directions when it allows some valid combos.
##
## Returns a new chain with the direction added if it allows for some valid
## combos according to the available combos, or the original chain
## unchanged.[br]
##
## Example:
## [codeblock]
## var combos := [
##     [ "forward, "upward" ], [ "forward", "forward", "forward" ]
## ]
##
## compose_combo([ "forward" ], "upward", valid_moves) \
##     == [ "forward", "upward" ]
##
## compose_combo([ "forward", "upward" ], "upward", valid_moves) \
##     == [ "forward", "upward" ]
##
## compose_combo([ "forward" ], "forward", valid_moves) \
##     == [ "forward", "forward" ] # Even though it's not a valid combo!
## [/codeblock]
static func compose_combo(
		current_chain: Array[StringName],
		direction: StringName,
		available_combos := []
	) -> Array[StringName]:
	var new_chain: Array[StringName] = current_chain.duplicate()
	new_chain.push_back(direction)

	var starts_with_new_chain := func(combo: Array) -> Bool:
		return combo.slice(0, new_chain.size()) == new_chain

	return (
			new_chain if available_combos.any(starts_with_new_chain)
			else current_chain
	)


## Get a list of all of the possible combos given a current chain of directions.
##
## Returns a new array containing the available combos, if any, starting with
## the specificied current chain. It includes all of the combos when the current
## chain is empty, and none if there is no possible combos.
##
## [br]Example:
## [codeblock]
## var combos := [
##     [ "forward", "forward" ], [ "forward", "upward" ], [ "backward" ]
## ]
##
## get_combo_predictions([], combos) \
##     == [[ "forward", "forward" ], [ "forward", "upward" ], [ "backward" ]]
## get_combo_predictions([ "forward" ], combos) \
##     == [[ "forward", "forward" ], [ "forward", "upward" ]]
## get_combo_predictions([ "backward" ], combos) \
##     == [["backward"]]
## get_combo_predictions([ "upward" ], combos) \
##     == [] # But this should never happen!
## [/codeblock]
static func get_combo_predictions(
		current_chain: Array[StringName], available_combos := []
	) -> Array:
	var starts_with_chain := func(combo: Array) -> Bool:
		return combo.slice(0, current_chain.size()) == current_chain

	return available_combos.filter(starts_with_chain)
