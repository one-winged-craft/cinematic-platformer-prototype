extends Node
class_name Root


var action_handler := ActionHandler.new()
var orientation := "right"
var state := "stand"

var _listen_input := true

@onready var player := $Player
@onready var tilemap := $TileMap


func _init() -> void:
	Utils.connect_or_fail(action_handler.combo_entered, _on_action_entered)
	Utils.connect_or_fail(action_handler.direct_entered, _on_action_entered)
	Utils.connect_or_fail(action_handler.action_cancelled,
			func(reason): print("Action cancelled (", reason, ")")
	)


func _input(event: InputEvent) -> void:
	if _listen_input:
		action_handler.process_input(event, state, orientation)


func _on_action_entered(current_state: StringName, action: StringName) -> void:
	prints("Action", action, "in state", current_state)
	var next_step: Dictionary = Move.next_step({
		state = current_state,
		action = action,
		orientation = orientation,
		cell_size = tilemap.cell_quadrant_size,
		on_finish = func() -> void:
			_listen_input = true,
	})
	next_step.apply.call(player, tilemap)
	_listen_input = false
	orientation = next_step.orientation
	state = next_step.state
